﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKControl : MonoBehaviour
{
    public bool ikActive = false;

    public GameObject rightHand;
    public GameObject playerLeftHand;
   
    public Animator animator;
    public ConfigurableJoint joint;

    public void Start()
    {
        joint = gameObject.GetComponent<ConfigurableJoint>();
    }

    public void Update()
    {
        Grab();
    }
    public void Grab()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Grab"))
        {
            rightHand.transform.position = playerLeftHand.transform.position;
            joint.connectedBody = playerLeftHand.GetComponent<Rigidbody>();
        }
    }
}
