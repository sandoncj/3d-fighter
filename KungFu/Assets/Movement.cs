﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float speed = 2;
    public float creepDis = 4;
    [SerializeField]
    private float basePos;

    public Transform target;
    public Animator control;
    public GameObject playerPos;
    public GameObject coreRot;

    public float attackCount1 = 0;
    public float attackCount2 = 0;
    public float attackTimer1 = 0;
    public float attackTimer2 = 0;

    public bool startTime1 = false;
    public bool startTime2 = false;
    void Update()
    {
        CombatMovement();
        CombatAttacks();
    }

    private void CombatMovement()
    {
        //gameObject.transform.transform.LookAt(new Vector3(target.localPosition.x, transform.position.y, target.localPosition.z));
        if (Input.GetKey(KeyCode.A))
        {
            control.SetBool("MoveForward", true);
        }       
        else
        {
            control.SetBool("MoveForward", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
            control.SetBool("MoveBackward", true);
        }
        else
        {
            control.SetBool("MoveBackward", false);
        } 

        if (Input.GetKey(KeyCode.S))
        {
            control.SetBool("MoveLeft", true);         
        }
        else
        {
            control.SetBool("MoveLeft", false);
        }

        if (Input.GetKey(KeyCode.W))
        {
            control.SetBool("MoveRight", true);
        }
        else
        {
            control.SetBool("MoveRight", false);
        }
    }
    private void CombatAttacks()
    {
        var currentState = control.GetCurrentAnimatorStateInfo(0);
   
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            attackCount1 += 1;
            startTime1 = true;
        }
        if (startTime1 == true)
        {
            attackTimer1 += Time.deltaTime;
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            attackCount2 += 1;
            startTime2 = true;
        }
        if (startTime2 == true)
        {
            attackTimer2 += Time.deltaTime;
        }



        //Punch
        if (attackCount1 == 1 && attackTimer1 > 0.2)
        {
            control.SetBool("SinglePunch", true);        
        }
        if (currentState.IsName("SinglePunch") && currentState.normalizedTime >= 1)
        {
            control.SetBool("SinglePunch", false);
            attackCount1 = 0;
            
        }
        if (currentState.IsName("SinglePunch") && currentState.normalizedTime >= 0.5)
        {
            startTime1 = false;
            attackTimer1 = 0;
        }
        //FastPunch
        if (attackCount1 >= 2 && attackTimer1 < 0.3)
        {
            control.SetBool("DoublePunch", true);
        }
        if (currentState.IsName("DoublePunch") && currentState.normalizedTime >= 1)
        {
            control.SetBool("DoublePunch", false);
            attackCount1 = 0;
            //Debug.Log(control.targetPosition);
        }
        if (currentState.IsName("DoublePunch") && currentState.normalizedTime >= 0.5)
        {
            startTime1 = false;
            attackTimer1 = 0;
        }

        //Kick
        if (attackCount2 == 1 && attackTimer2 > 0.2)
        {
            control.SetBool("Kick", true);
        }
        if (currentState.IsName("Kick") && currentState.normalizedTime >= 1)
        {
            control.SetBool("Kick", false);
            attackCount2 = 0;
        }
        if (currentState.IsName("Kick") && currentState.normalizedTime >= 0.5)
        {
            startTime2 = false;
            attackTimer2 = 0;
        }

        //PunchFlurry
        if (attackCount2 == 2 && attackTimer1 < 0.2)
        {
            control.SetBool("PunchFlurry", true);
        }      
        if (currentState.IsName("PunchFlurry") && currentState.normalizedTime >= 1)
        {
            control.SetBool("PunchFlurry", false);
            attackCount2 = 0;
        }
        if (currentState.IsName("PunchFlurry") && currentState.normalizedTime >= 0.5)
        {
            startTime2 = false;
            attackTimer2 = 0;
        }

        //Grab
        if (Input.GetKey(KeyCode.E))
        {
            control.SetBool("Grab", true);            
        }
        if (currentState.IsName("Grab") && currentState.normalizedTime >= 1)
        {
            control.SetBool("Grab", false);            
        }

        


    }


}
