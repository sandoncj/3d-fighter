﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combos : MonoBehaviour
{
    public Movement player;

    private void Start()
    {
        player = gameObject.GetComponent<Movement>();
    }
    private void Update()
    {
        PunchFlurry();
        DoublePunch();
    }
    private void PunchFlurry()
    {
        #region To Double Punch
        if (player.attackCount1 >= 2 && player.attackTimer1 < 0.3)
        {
            player.attackCount2 = 0;
            player.attackTimer2 = 0;
            player.control.SetBool("PunchFlurry", false);
            player.control.SetBool("FlurryToDouble", true);
        }
        player.control.SetBool("FlurryToDouble", false);
        
        #endregion
        #region To Single Punch
        if (player.attackCount1 == 1 && player.attackTimer1 > 0.2)
        {
            player.attackCount2 = 0;
            player.attackTimer2 = 0;
            player.control.SetBool("PunchFlurry", false);
            player.control.SetBool("FlurryToSingle", true);
        }     
        player.control.SetBool("FlurryToSingle", false);
        
        #endregion
        #region To Grab
        if (Input.GetKeyDown(KeyCode.E))
        {            
            player.attackCount2 = 0;
            player.attackTimer2 = 0;
            player.control.SetBool("PunchFlurry", false);
            player.control.SetBool("FlurryToGrab", true);
        }       
        player.control.SetBool("FlurryToGrab", false);
        
        #endregion
    }

    private void DoublePunch()
    {
        #region To Flurry
        if (player.attackCount2 >= 2 && player.attackTimer2 < 0.3)
        {
            player.attackCount1 = 0;
            player.attackTimer1 = 0;
            player.control.SetBool("DoublePunch", false);
            player.control.SetBool("DoubleToFlurry", true);
        }
        player.control.SetBool("DoubleToFlurry", false);

        #endregion
        #region To Grab
        if (Input.GetKeyDown(KeyCode.E))
        {
            player.attackCount1 = 0;
            player.attackTimer1 = 0;
            player.control.SetBool("DoublePunch", false);
            player.control.SetBool("DoubleToGrab", true);
        }
        player.control.SetBool("DoubleToGrab", false);
        #endregion
    }

    private void SinglePunch()
    {
        #region To Double Punch
        if (player.attackCount1 >= 2 && player.attackTimer1 < 0.3)
        {
            player.attackCount1 = 0;
            player.attackTimer1 = 0;
            player.control.SetBool("SinglePunch", false);
            player.control.SetBool("SingleToDouble", true);
        }
        player.control.SetBool("SingleToDouble", false);

        #endregion
        #region To Flurry
        if (player.attackCount2 >= 2 && player.attackTimer2 < 0.2)
        {
            player.attackCount1 = 0;
            player.attackTimer1 = 0;
            player.control.SetBool("SinglePunch", false);
            player.control.SetBool("SingleToFlurry", true);
        }
        player.control.SetBool("SingleToFlurry", false);

        #endregion
        #region To Grab
        if (Input.GetKeyDown(KeyCode.E))
        {
            player.attackCount1 = 0;
            player.attackTimer1 = 0;
            player.control.SetBool("SinglePunch", false);
            player.control.SetBool("SingleToGrab", true);
        }
        player.control.SetBool("SingleToGrab", false);

        #endregion
    }



}
